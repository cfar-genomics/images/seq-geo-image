# Notes

# Pulling Images

## Pulling for Development
The following will pull the most recently built image into an "images" subdirectory of your /work directory, (/work/YOUR_NETID/images). 

```
IMAGEDIR=/work/${USER}/images
mkdir -p $IMAGEDIR
singularity pull --force --dir $IMAGEDIR oras://gitlab-registry.oit.duke.edu//cfar-genomics/images/seq-geo-image:latest
```

## Pulling for Production
The following command will pull the image with the tag **TAG_NAME** into the directory for production images (/opt/apps/community/od_chsi_rstudio). You can only do this if you have permissions to write to this directory. You must replace **TAG_NAME** with the commit tag of the image you want to pull:

`singularity pull --force --dir /opt/apps/community/od_chsi_rstudio oras://gitlab-registry.oit.duke.edu//cfar-genomics/images/seq-geo-image:TAG_NAME`

